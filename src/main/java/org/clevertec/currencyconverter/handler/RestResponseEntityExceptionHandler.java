package org.clevertec.currencyconverter.handler;

import lombok.RequiredArgsConstructor;
import org.clevertec.currencyconverter.constants.MessageNames;
import org.clevertec.currencyconverter.exceptions.IncorrectCurrencyNameException;
import org.clevertec.currencyconverter.exceptions.NegativeNumberException;
import org.clevertec.currencyconverter.exceptions.StringToXMLGregorianCalendarConversionException;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Locale;

@ControllerAdvice
@RequiredArgsConstructor
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    private final MessageSource messageSource;

    @ExceptionHandler(NegativeNumberException.class)
    protected ResponseEntity<Object> handleNegativeNumberException(NegativeNumberException e) {
        return new ResponseEntity<>(messageSource.getMessage(MessageNames.NEGATIVE_CONVERSION_AMOUNT_MESSAGE,
                new Object[]{e.getMessage()}, Locale.UK) ,HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(StringToXMLGregorianCalendarConversionException.class)
    protected ResponseEntity<Object> handleStringToXMLGregorianCalendarConversionException(
            StringToXMLGregorianCalendarConversionException e) {
        return new ResponseEntity<>(messageSource.getMessage(MessageNames.INCORRECT_DATE_FORMAT_MESSAGE,
                new Object[]{e.getMessage()}, Locale.UK) ,HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(IncorrectCurrencyNameException.class)
    protected ResponseEntity<Object> handleIncorrectCurrencyNameException(IncorrectCurrencyNameException e){
        return new ResponseEntity<>(messageSource.getMessage(MessageNames.INCORRECT_CURRENCY_NAME_MESSAGE,
                new Object[]{e.getMessage()}, Locale.UK), HttpStatus.BAD_REQUEST);
    }
}
