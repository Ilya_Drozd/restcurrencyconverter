package org.clevertec.currencyconverter.controller;

import lombok.RequiredArgsConstructor;
import org.clevertec.currencyconverter.dto.CurrencyConverterData;
import org.clevertec.currencyconverter.dto.CurrencyData;
import org.clevertec.currencyconverter.service.CurrencyService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping(value = "/currencies")
@RequiredArgsConstructor
public class CurrencyController {

    private final CurrencyService currencyService;

    @PostMapping(value = "/conversion/amount", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BigDecimal> getConversionAmount(@RequestBody CurrencyConverterData currencyConverterData) {
        return ResponseEntity.ok(currencyService.getConversionAmount(currencyConverterData));
    }

    @PostMapping(value = "/conversion/rate", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BigDecimal> getConversionRate(@RequestBody CurrencyConverterData currencyConverterData) {
        return ResponseEntity.ok(currencyService.getConversionRate(currencyConverterData));
    }

    @PostMapping(value = "currency/info", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getCultureInfo(@RequestBody CurrencyData currencyData) {
        return ResponseEntity.ok(currencyService.getCultureInfo(currencyData));
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<String>> getCurrencies() {
        return ResponseEntity.ok(currencyService.getCurrencies());
    }

    @PostMapping(value = "currency/rate", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BigDecimal> getCurrencyRate(@RequestBody CurrencyData currencyData) {
        return ResponseEntity.ok(currencyService.getCurrencyRate(currencyData));
    }

    @GetMapping(value = "/date", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<XMLGregorianCalendar> getLastUpdateDate(){
        return ResponseEntity.ok(currencyService.getLastUpdateDate());
    }

}
