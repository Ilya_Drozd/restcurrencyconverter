package org.clevertec.currencyconverter.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.clevertec.currencyconverter.constants.MessageNames;
import org.clevertec.currencyconverter.dto.CurrencyConverterData;
import org.clevertec.currencyconverter.dto.CurrencyData;
import org.clevertec.currencyconverter.exceptions.IncorrectCurrencyNameException;
import org.clevertec.currencyconverter.exceptions.NegativeNumberException;
import org.clevertec.currencyconverter.exceptions.StringToXMLGregorianCalendarConversionException;
import org.clevertec.currencyconverter.repository.CurrencyRepository;
import org.clevertec.currencyconverter.service.CurrencyService;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

@Service
@Slf4j
@RequiredArgsConstructor
public class CurrencyServiceImpl implements CurrencyService {

    private final CurrencyRepository repository;
    private final MessageSource messageSource;

    @Override
    public BigDecimal getConversionAmount(CurrencyConverterData currencyConverterData) {
        String currencyFrom = currencyConverterData.getCurrencyFrom();
        String currencyTo = currencyConverterData.getCurrencyTo();
        XMLGregorianCalendar rateDate = convertStringToXMLGregorianCalendar(currencyConverterData.getRateDate());
        BigDecimal amount = currencyConverterData.getAmount();

        if(amount.signum() != -1){
            BigDecimal result = repository.getConversionAmount(currencyFrom, currencyTo, rateDate, amount);

            if(result.compareTo(new BigDecimal(0)) != 0 || amount.compareTo(new BigDecimal(0)) == 0){
                log.info(messageSource.getMessage(MessageNames.CONVERSION_AMOUNT_MESSAGE,
                        new Object[]{currencyFrom, currencyTo, amount, result.toString()}, Locale.UK));
                return result;
            } else {
                log.error(messageSource.getMessage(MessageNames.INCORRECT_CURRENCY_NAME_MESSAGE,
                        new Object[]{currencyFrom + MessageNames.COMMA + currencyTo}, Locale.UK));
                throw new IncorrectCurrencyNameException(currencyFrom + MessageNames.COMMA + currencyTo);
            }

        } else {
            log.error(messageSource.getMessage(MessageNames.NEGATIVE_CONVERSION_AMOUNT_MESSAGE,
                    new Object[]{amount}, Locale.UK));
            throw new NegativeNumberException(amount.toString());
        }
    }

    @Override
    public BigDecimal getConversionRate(CurrencyConverterData currencyConverterData) {
        String currencyFrom = currencyConverterData.getCurrencyFrom();
        String currencyTo = currencyConverterData.getCurrencyTo();
        XMLGregorianCalendar rateDate = convertStringToXMLGregorianCalendar(currencyConverterData.getRateDate());

        BigDecimal result = repository.getConversionRate(currencyFrom, currencyTo, rateDate);

        if(result.compareTo(new BigDecimal(0)) != 0){
            log.info(messageSource.getMessage(MessageNames.CONVERSION_RATE_MESSAGE,
                    new Object[]{currencyFrom, currencyTo, rateDate, result.toString()}, Locale.UK));
            return result;
        } else {
            log.error(messageSource.getMessage(MessageNames.INCORRECT_CURRENCY_NAME_MESSAGE,
                    new Object[]{currencyFrom + MessageNames.COMMA + currencyTo}, Locale.UK));
            throw new IncorrectCurrencyNameException(currencyFrom + MessageNames.COMMA + currencyTo);
        }
    }

    @Override
    public String getCultureInfo(CurrencyData currencyData) {
        String currency = currencyData.getCurrency();

        String result = repository.getCultureInfo(currency);

        if(!result.equals("")){
            log.info(messageSource.getMessage(MessageNames.CULTURE_INFO_MESSAGE,
                    new Object[]{currency, result}, Locale.UK));
            return result;
        } else {
            log.error(messageSource.getMessage(MessageNames.INCORRECT_CURRENCY_NAME_MESSAGE,
                    new Object[]{currency}, Locale.UK));
            throw new IncorrectCurrencyNameException(currency);
        }
    }

    @Override
    public List<String> getCurrencies() {
        List<String> result = repository.getCurrencies();

        log.info(messageSource.getMessage(MessageNames.CURRENCIES_MESSAGE,
                new Object[]{result}, Locale.UK));
        return result;
    }

    @Override
    public BigDecimal getCurrencyRate(CurrencyData currencyData) {
        String currency = currencyData.getCurrency();
        XMLGregorianCalendar rateDate = convertStringToXMLGregorianCalendar(currencyData.getRateDate());

        BigDecimal result = repository.getCurrencyRate(currency, rateDate);

        if(result.compareTo(new BigDecimal(0)) != 0){
            log.info(messageSource.getMessage(MessageNames.CURRENCY_RATE_MESSAGE,
                    new Object[]{currency, rateDate, result.toString()}, Locale.UK));
            return result;
        } else {
            log.error(messageSource.getMessage(MessageNames.INCORRECT_CURRENCY_NAME_MESSAGE,
                    new Object[]{currency}, Locale.UK));
            throw new IncorrectCurrencyNameException(currency);
        }
    }

    @Override
    public XMLGregorianCalendar getLastUpdateDate() {
        XMLGregorianCalendar result = repository.getLastUpdateDate();
        log.info(messageSource.getMessage(MessageNames.LAST_UPDATE_DATE_MESSAGE,
                new Object[]{result}, Locale.UK));
        return result;
    }

    private XMLGregorianCalendar convertStringToXMLGregorianCalendar(String line){
        try {
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(line);
        } catch (DatatypeConfigurationException | IllegalArgumentException e) {
            log.error(messageSource.getMessage(MessageNames.INCORRECT_DATE_FORMAT_MESSAGE,
                    new Object[]{line}, Locale.UK));
            throw new StringToXMLGregorianCalendarConversionException(line);
        }
    }
}
