
package org.clevertec.currencyconverter.soap.com.dss.converter;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetConversionRateResult" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getConversionRateResult"
})
@XmlRootElement(name = "GetConversionRateResponse", namespace = "http://tempuri.org/")
public class GetConversionRateResponse {

    @XmlElement(name = "GetConversionRateResult", namespace = "http://tempuri.org/", required = true)
    protected BigDecimal getConversionRateResult;

    /**
     * Gets the value of the getConversionRateResult property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getGetConversionRateResult() {
        return getConversionRateResult;
    }

    /**
     * Sets the value of the getConversionRateResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setGetConversionRateResult(BigDecimal value) {
        this.getConversionRateResult = value;
    }

}
