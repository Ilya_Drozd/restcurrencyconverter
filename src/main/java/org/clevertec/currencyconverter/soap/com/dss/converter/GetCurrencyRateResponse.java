
package org.clevertec.currencyconverter.soap.com.dss.converter;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetCurrencyRateResult" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCurrencyRateResult"
})
@XmlRootElement(name = "GetCurrencyRateResponse", namespace = "http://tempuri.org/")
public class GetCurrencyRateResponse {

    @XmlElement(name = "GetCurrencyRateResult", namespace = "http://tempuri.org/", required = true)
    protected BigDecimal getCurrencyRateResult;

    /**
     * Gets the value of the getCurrencyRateResult property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getGetCurrencyRateResult() {
        return getCurrencyRateResult;
    }

    /**
     * Sets the value of the getCurrencyRateResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setGetCurrencyRateResult(BigDecimal value) {
        this.getCurrencyRateResult = value;
    }

}
