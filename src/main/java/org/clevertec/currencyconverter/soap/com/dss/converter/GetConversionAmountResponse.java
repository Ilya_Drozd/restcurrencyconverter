
package org.clevertec.currencyconverter.soap.com.dss.converter;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetConversionAmountResult" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getConversionAmountResult"
})
@XmlRootElement(name = "GetConversionAmountResponse", namespace = "http://tempuri.org/")
public class GetConversionAmountResponse {

    @XmlElement(name = "GetConversionAmountResult", namespace = "http://tempuri.org/", required = true)
    protected BigDecimal getConversionAmountResult;

    /**
     * Gets the value of the getConversionAmountResult property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getGetConversionAmountResult() {
        return getConversionAmountResult;
    }

    /**
     * Sets the value of the getConversionAmountResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setGetConversionAmountResult(BigDecimal value) {
        this.getConversionAmountResult = value;
    }

}
