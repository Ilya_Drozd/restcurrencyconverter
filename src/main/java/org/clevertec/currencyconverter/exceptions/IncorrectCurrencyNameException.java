package org.clevertec.currencyconverter.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class IncorrectCurrencyNameException extends RuntimeException{
    private String message;
}
