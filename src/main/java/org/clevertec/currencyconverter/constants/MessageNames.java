package org.clevertec.currencyconverter.constants;

public class MessageNames {

    public static final String CONVERSION_AMOUNT_MESSAGE = "message.conversionAmountMessage";
    public static final String NEGATIVE_CONVERSION_AMOUNT_MESSAGE = "message.negativeConversionAmountMessage";
    public static final String CONVERSION_RATE_MESSAGE = "message.conversionRateMessage";
    public static final String CULTURE_INFO_MESSAGE = "message.cultureInfoMessage";
    public static final String CURRENCIES_MESSAGE = "message.currenciesMessage";
    public static final String CURRENCY_RATE_MESSAGE = "message.currencyRateMessage";
    public static final String LAST_UPDATE_DATE_MESSAGE = "message.lastUpdateDateMessage";
    public static final String INCORRECT_DATE_FORMAT_MESSAGE = "message.incorrectDateFormatMessage";
    public static final String INCORRECT_CURRENCY_NAME_MESSAGE = "message.incorrectCurrencyNameMessage";

    public static final String UTF_8 = "UTF-8";

    public static final String COMMA = ", ";

}
