package org.clevertec.currencyconverter.repository;

import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.util.List;

public interface CurrencyRepository {
    BigDecimal getConversionAmount(String currencyFrom, String currencyTo, XMLGregorianCalendar rateDate,
                                   BigDecimal amount);
    BigDecimal getConversionRate(String currencyFrom, String currencyTo, XMLGregorianCalendar rateDate);
    String getCultureInfo(String currency);
    List<String> getCurrencies();
    BigDecimal getCurrencyRate(String currency, XMLGregorianCalendar rateDate);
    XMLGregorianCalendar getLastUpdateDate();

}
