package org.clevertec.currencyconverter.repository.impl;

import lombok.Setter;
import org.clevertec.currencyconverter.repository.CurrencyRepository;
import org.clevertec.currencyconverter.soap.com.dss.converter.Converter;
import org.clevertec.currencyconverter.soap.com.dss.converter.ConverterSoap;
import org.springframework.stereotype.Component;

import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.util.List;

@Component
public class CurrencyRepositoryImpl implements CurrencyRepository {

    @Setter
    private ConverterSoap converterSoap = new Converter().getConverterSoap();

    @Override
    public BigDecimal getConversionAmount(String currencyFrom, String currencyTo, XMLGregorianCalendar rateDate, BigDecimal amount) {
        return converterSoap.getConversionAmount(currencyFrom, currencyTo, rateDate, amount);
    }

    @Override
    public BigDecimal getConversionRate(String currencyFrom, String currencyTo, XMLGregorianCalendar rateDate) {
        return converterSoap.getConversionRate(currencyFrom, currencyTo, rateDate);
    }

    @Override
    public String getCultureInfo(String currency) {
        return converterSoap.getCultureInfo(currency);
    }

    @Override
    public List<String> getCurrencies() {
        return converterSoap.getCurrencies().getString();
    }

    @Override
    public BigDecimal getCurrencyRate(String currency, XMLGregorianCalendar rateDate) {
        return converterSoap.getCurrencyRate(currency, rateDate);
    }

    @Override
    public XMLGregorianCalendar getLastUpdateDate() {
        return converterSoap.getLastUpdateDate();
    }
}
