package org.clevertec.currencyconverter.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.clevertec.currencyconverter.dto.CurrencyConverterData;
import org.clevertec.currencyconverter.dto.CurrencyData;
import org.clevertec.currencyconverter.service.CurrencyService;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.xml.datatype.DatatypeFactory;
import java.math.BigDecimal;
import java.util.ArrayList;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest
public class CurrencyControllerTest {

    private static CurrencyConverterData currencyConverterData;
    private static CurrencyData currencyData;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private CurrencyService currencyService;

    @BeforeClass
    public static void initData() {
        String currencyFrom = "EUR";
        String currencyTo = "RUB";
        String currency = "USD";
        String rateDateStr = "2020-01-27T00:00:00";
        BigDecimal amount = new BigDecimal("5");

        currencyConverterData = new CurrencyConverterData(currencyFrom, currencyTo, rateDateStr, amount);
        currencyData = new CurrencyData(currency, rateDateStr);
    }

    @Test
    public void getConversionAmountAndExpectStatusIsOk() throws Exception {
        given(this.currencyService.getConversionAmount(currencyConverterData)).willReturn(new BigDecimal(5));

        this.mockMvc.perform(post("http://localhost:8091/currencies/conversion/amount")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(currencyConverterData)))
                .andExpect(status().isOk());

    }

    @Test
    public void getConversionRateAndExpectStatusIsOk() throws Exception {
        given(this.currencyService.getConversionRate(currencyConverterData)).willReturn(new BigDecimal(5));

        this.mockMvc.perform(post("http://localhost:8091/currencies/conversion/rate")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(currencyConverterData)))
                .andExpect(status().isOk());
    }

    @Test
    public void getCultureInfoAndExpectStatusIsOk() throws Exception {
        given(this.currencyService.getCultureInfo(currencyData)).willReturn("sv-FI");

        this.mockMvc.perform(post("http://localhost:8091/currencies/currency/info")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(currencyData)))
                .andExpect(status().isOk());
    }

    @Test
    public void getCurrencies() throws Exception {
        given(this.currencyService.getCurrencies()).willReturn(new ArrayList<>());

        this.mockMvc.perform(get("http://localhost:8091/currencies"))
                .andExpect(status().isOk());
    }

    @Test
    public void getCurrencyRate() throws Exception {
        given(this.currencyService.getCurrencyRate(currencyData)).willReturn(new BigDecimal(5));

        this.mockMvc.perform(post("http://localhost:8091/currencies/currency/rate")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(currencyData)))
                .andExpect(status().isOk());
    }

    @Test
    public void getLastUpdateDate() throws Exception {
        given(this.currencyService.getLastUpdateDate())
                .willReturn(DatatypeFactory.newInstance().newXMLGregorianCalendar("2020-01-27T00:00:00"));

        this.mockMvc.perform(get("http://localhost:8091/currencies/date"))
                .andExpect(status().isOk());
    }
}