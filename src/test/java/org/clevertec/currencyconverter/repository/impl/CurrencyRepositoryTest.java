package org.clevertec.currencyconverter.repository.impl;


import org.clevertec.currencyconverter.soap.com.dss.converter.ArrayOfString;
import org.clevertec.currencyconverter.soap.com.dss.converter.ConverterSoap;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CurrencyRepositoryTest {

    private static String currencyFrom;
    private static String currencyTo;
    private static String currency;
    private static XMLGregorianCalendar rateDate;
    private static BigDecimal amount;

    @Autowired
    private CurrencyRepositoryImpl currencyRepository;

    @Mock
    private ConverterSoap converterSoap;

    @Before
    public void initMock() {
        currencyRepository.setConverterSoap(converterSoap);
    }

    @BeforeClass
    public static void initData() throws DatatypeConfigurationException {
        currencyFrom = "EUR";
        currencyTo = "RUB";
        currency = "USD";
        rateDate = DatatypeFactory.newInstance().newXMLGregorianCalendar("2020-01-27T00:00:00");
        ;
        amount = new BigDecimal("5");
    }

    @Test
    public void getConversionAmount() {
        String result = "352";
        given(this.converterSoap.getConversionAmount(currencyFrom, currencyTo, rateDate, amount)).willReturn(new BigDecimal(result));

        BigDecimal conversionAmount = currencyRepository.getConversionAmount(currencyFrom, currencyTo, rateDate, amount);
        Mockito.verify(converterSoap).getConversionAmount(currencyFrom, currencyTo, rateDate, amount);
        assertEquals(conversionAmount, new BigDecimal(result));
    }

    @Test
    public void getConversionRate() {
        String result = "62.625";
        given(this.converterSoap.getConversionRate(currencyFrom, currencyTo, rateDate)).willReturn(new BigDecimal(result));

        BigDecimal rate = currencyRepository.getConversionRate(currencyFrom, currencyTo, rateDate);
        Mockito.verify(converterSoap).getConversionRate(currencyFrom, currencyTo, rateDate);
        assertEquals(rate, new BigDecimal(result));
    }

    @Test
    public void getCultureInfo() {
        String result = "sv-FI";
        given(this.converterSoap.getCultureInfo(currency)).willReturn(result);

        String info = currencyRepository.getCultureInfo(currency);
        Mockito.verify(converterSoap).getCultureInfo(currency);
        assertEquals(info, result);
    }

    @Test
    public void getCurrencies() {
        given(this.converterSoap.getCurrencies()).willReturn(new ArrayOfString());

        currencyRepository.getCurrencies();
        Mockito.verify(converterSoap).getCurrencies();
    }

    @Test
    public void getCurrencyRate() {
        String result = "1.1025";
        given(this.converterSoap.getCurrencyRate(currency, rateDate)).willReturn(new BigDecimal(result));

        BigDecimal rate = currencyRepository.getCurrencyRate(currency, rateDate);
        Mockito.verify(converterSoap).getCurrencyRate(currency, rateDate);
        assertEquals(rate, new BigDecimal(result));
    }

    @Test
    public void getLastUpdateDate() {
        given(this.converterSoap.getLastUpdateDate()).willReturn(rateDate);

        currencyRepository.getLastUpdateDate();
        Mockito.verify(converterSoap).getLastUpdateDate();
    }
}