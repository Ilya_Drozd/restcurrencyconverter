package org.clevertec.currencyconverter.service.impl;

import org.clevertec.currencyconverter.dto.CurrencyConverterData;
import org.clevertec.currencyconverter.dto.CurrencyData;
import org.clevertec.currencyconverter.exceptions.NegativeNumberException;
import org.clevertec.currencyconverter.exceptions.StringToXMLGregorianCalendarConversionException;
import org.clevertec.currencyconverter.repository.CurrencyRepository;
import org.clevertec.currencyconverter.service.CurrencyService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CurrencyServiceTest {

    private String currencyFrom;
    private String currencyTo;
    private String currency;
    private XMLGregorianCalendar rateDate;
    private BigDecimal amount;

    private CurrencyConverterData currencyConverterData;
    private CurrencyData currencyData;

    @Autowired
    private CurrencyService currencyService;

    @MockBean
    private CurrencyRepository currencyRepository;

    @Before
    public void initData() throws DatatypeConfigurationException {
        currencyFrom = "EUR";
        currencyTo = "RUB";
        currency = "USD";
        String rateDateStr = "2020-01-27T00:00:00";
        rateDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(rateDateStr);
        amount = new BigDecimal("5");

        currencyConverterData = new CurrencyConverterData(currencyFrom, currencyTo, rateDateStr, amount);
        currencyData = new CurrencyData(currency, rateDateStr);
    }

    @Test
    public void getConversionAmount() {
        String result = "352";
        given(this.currencyRepository.getConversionAmount(currencyFrom, currencyTo, rateDate, amount))
                .willReturn(new BigDecimal(result));

        BigDecimal conversionAmount = currencyService.getConversionAmount(currencyConverterData);

        Mockito.verify(currencyRepository).getConversionAmount(currencyFrom, currencyTo, rateDate, amount);
        assertEquals(conversionAmount, new BigDecimal(result));
    }

    @Test(expected = NegativeNumberException.class)
    public void getConversionAmountWithNegativeNumberException() {
        currencyConverterData.setAmount(new BigDecimal(-5));
        currencyService.getConversionAmount(currencyConverterData);
    }

    @Test(expected = StringToXMLGregorianCalendarConversionException.class)
    public void getConversionAmountWithStringToXMLGregorianCalendarConversionException() {
        String badRateDate = "2020.01.27";
        currencyConverterData.setRateDate(badRateDate);

        currencyService.getConversionAmount(currencyConverterData);
    }

    @Test
    public void getConversionRate() {
        String result = "62.625";
        given(this.currencyRepository.getConversionRate(currencyFrom, currencyTo, rateDate))
                .willReturn(new BigDecimal(result));

        BigDecimal conversionRate = currencyService.getConversionRate(currencyConverterData);

        Mockito.verify(currencyRepository).getConversionRate(currencyFrom, currencyTo, rateDate);
        assertEquals(conversionRate, new BigDecimal(result));
    }

    @Test
    public void getCultureInfo() {
        String result = "sv-FI";
        given(this.currencyRepository.getCultureInfo(currency)).willReturn(result);

        String info = currencyService.getCultureInfo(currencyData);

        Mockito.verify(currencyRepository).getCultureInfo(currency);
        assertEquals(info, result);
    }

    @Test
    public void getCurrencies() {
        given(this.currencyRepository.getCurrencies()).willReturn(new ArrayList<String>());

        currencyService.getCurrencies();
        Mockito.verify(currencyRepository).getCurrencies();
    }

    @Test
    public void getCurrencyRate() {
        String result = "1.1025";
        given(this.currencyRepository.getCurrencyRate(currency, rateDate)).willReturn(new BigDecimal(result));

        BigDecimal rate = currencyService.getCurrencyRate(currencyData);

        Mockito.verify(currencyRepository).getCurrencyRate(currency, rateDate);
        assertEquals(rate, new BigDecimal(result));
    }

    @Test
    public void getLastUpdateDate() {
        given(this.currencyRepository.getLastUpdateDate()).willReturn(rateDate);

        currencyService.getLastUpdateDate();
        Mockito.verify(currencyRepository).getLastUpdateDate();
    }
}