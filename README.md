# Currency converter

## Description
REST service for working with SOAP

## REST-services:
    
#### Conversion amount
#### POST [http://localhost:8091/currencies/conversion/amount](http://localhost:8091/currencies/conversion/amount)
#### getting conversion amount of two currencies

        Example of request body:
        {
            "currencyFrom": "USD",
        	"currencyTo": "RUB",
        	"rateDate": "2020-01-27T00:00:00",
        	"amount": "10"
        }

---

#### Conversion rate
#### POST [http://localhost:8091/currencies/conversion/rate](http://localhost:8091/currencies/conversion/rate)
#### getting conversion rate of two currencies

        Example of request body:
        {
            "currencyFrom": "USD",
        	"currencyTo": "RUB",
        	"rateDate": "2020-01-27T00:00:00"
        }

---

#### Culture info
#### POST [http://localhost:8091/currencies/currency/info](http://localhost:8091/currencies/currency/info)
#### getting culture info of currency

        Example of request body:
        {
        	"currency": "USD"
        }

---

#### Get currencies
#### GET [http://localhost:8091/currencies](http://localhost:8091/currencies)
#### receiving all currencies

---

#### Currency rate
#### POST [http://localhost:8091/currencies/currency/rate](http://localhost:8091/currencies/currency/rate)
#### getting rate of currency

        Example of request body:
        {
        	"currency": "RUB",
        	"rateDate": "2020-01-27T00:00:00"
        }

---

#### Last update date
#### GET [http://localhost:8091/currencies/date](http://localhost:8091/currencies/date)
#### receiving last update date 

---